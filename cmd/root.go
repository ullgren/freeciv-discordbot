package cmd

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/bwmarrin/discordgo"
	"github.com/fsnotify/fsnotify"
	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/ullgren/freeciv-discordbot/internal"
	"gitlab.com/ullgren/freeciv-discordbot/internal/discord"
)

var cfgFile string

var rootCmd = &cobra.Command{
	Use:       "freeciv-discordbot",
	Short:     "Discord bot for freeciv server",
	Long:      `This discord bot monitors status, currently only save files, of a freeciv server and sends information to Discord on any updates.`,
	Example:   "./freeciv-discordbot",
	ValidArgs: []string{},
	Run: func(cmd *cobra.Command, args []string) {

		var session *discordgo.Session
		var client *discord.DiscordClient

		var saveDir string = viper.GetString("savedir")

		session, err := discordgo.New("Bot " + viper.GetString("discord.token"))
		if err != nil {
			log.Fatalf("error creating discord session %+v", err)
		}
		client, err = discord.NewDiscordClient(
			session,
			viper.GetString("discord.guild"),
			viper.GetString("discord.channel"))
		if err != nil {
			log.Fatalf("error creating discord client %+v", err)
		}
		if saveDir == "" {
			saveDir, err = homedir.Dir()
			if err != nil {
				log.Fatalf("error fetching users home directory %+v", err)
			}
		}

		watcher, err := fsnotify.NewWatcher()
		if err != nil {
			log.Fatalf("NewWatcher failed: %+v\n", err)
		}
		defer watcher.Close()

		done := make(chan os.Signal, 1)
		go func() {
			defer close(done)

			for {
				select {
				case event, ok := <-watcher.Events:
					if !ok {
						return
					}
					if event.Op == fsnotify.Create {
						if savename, turn, year, filenameOk := internal.ParseAutoSaveFileName(event.Name); filenameOk {
							log.Printf("new savename %s turn %d year %d\n", savename, turn, year)
							messageID, err := client.SendMessage(fmt.Sprintf("New turn %d year %d", turn, year))
							if err != nil {
								log.Printf("error %+v\n", err)
								continue
							} else {
								log.Printf("successfully send message %s\n", messageID)
							}
						} else {
							log.Printf("failed to parse filename %s\n", event.Name)
						}
					}
				case err, ok := <-watcher.Errors:
					if !ok {
						return
					}
					log.Printf("error: %+v\n", err)
				}
			}
		}()

		log.Printf("Adding watcher for %s\n", saveDir)
		err = watcher.Add(saveDir)
		if err != nil {
			log.Fatal("Add failed:", err)
		}
		signal.Notify(done, syscall.SIGINT, syscall.SIGTERM, syscall.SIGSEGV, syscall.SIGHUP)
		<-done
		client.CloseSession()

	},
}

// Execute will execute the rootCmd
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.freecivbot.yaml)")

	rootCmd.Flags().StringP("token", "t", "", "Discord bot token used to login to Discord")
	cobra.MarkFlagRequired(rootCmd.Flags(), "token")
	viper.BindPFlag("discord.token", rootCmd.Flags().Lookup("token"))

	rootCmd.Flags().StringP("guild", "g", "", "Name of the guild in which to look for the channel")
	cobra.MarkFlagRequired(rootCmd.Flags(), "guild")
	viper.BindPFlag("discord.guild", rootCmd.Flags().Lookup("guild"))

	rootCmd.Flags().StringP("channel", "c", "", "Name of the channel to post updates to")
	cobra.MarkFlagRequired(rootCmd.Flags(), "channel")
	viper.BindPFlag("discord.channel", rootCmd.Flags().Lookup("channel"))

	rootCmd.Flags().StringP("savedir", "s", "", "Directory to watch for new save files")
	viper.BindPFlag("savedir", rootCmd.Flags().Lookup("savedir"))

}

func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in home directory with name ".freecivbot" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".freecivbot")
	}

	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}
