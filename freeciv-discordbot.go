/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package main

import (
	"gitlab.com/ullgren/freeciv-discordbot/cmd"
)

func main() {
	cmd.Execute()
}
