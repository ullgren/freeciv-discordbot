package discord

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
	"github.com/pkg/errors"
)

type DiscordClient struct {
	session   *discordgo.Session
	channelID string
}

func NewDiscordClient(session *discordgo.Session, guildName, channelName string) (c *DiscordClient, err error) {
	var client DiscordClient
	client.session = session

	client.channelID, err = client.lookupFreecivTextChannelID(guildName, channelName)
	if err != nil {
		return nil, errors.Wrap(err, "failed lookup channel ID")
	}
        client.session.AddHandler(client.newMessage)
        client.session.Open()
	return &client, nil
}

func (client *DiscordClient) SendMessage(message string) (messageId string, err error) {
	msg, err := client.session.ChannelMessageSend(client.channelID,
		message)
	if err != nil {
		return "", errors.Wrap(err, "failed to send message")
	}
	return msg.ID, nil
}

func (client *DiscordClient) newMessage(s *discordgo.Session, m *discordgo.MessageCreate) {
       return
}

func (client *DiscordClient) CloseSession() error {
	return client.session.Close()
}

//  lookupFreecivTextChannelID will use the provided session to lookup the ID of the channel channelName in guild with the name guildName
func (client *DiscordClient) lookupFreecivTextChannelID(guildName, channelName string) (string, error) {
	guilds, err := client.session.UserGuilds(100, "", "")
	if err != nil {
		return "", errors.Wrap(err, "failed to fetch guilds")
	}

	for _, guild := range guilds {
		if guild.Name == guildName {
			channels, err := client.session.GuildChannels(guild.ID)

			if err != nil {
				return "", errors.Wrapf(err, "failed to fetch guild channels for guild %s", guild.Name)
			}
			for _, channel := range channels {
				if channel.Type == discordgo.ChannelTypeGuildText && channel.Name == channelName {
					return channel.ID, nil
				}
			}
		}
	}
	return "", fmt.Errorf("no channel found")
}
