package internal

import (
	"regexp"
	"strconv"
)

// ParseAutoSaveFileName checks if the given file name matches the pattern {savename}-T{turn}-Y{year}-{auto/manual}.save.[bz2|xz]
// If it does it returns ok == true and the savename, turn and year part of the file name.
//  Otherwise ok is returned as false
func ParseAutoSaveFileName(filename string) (savename string, turn, year int, ok bool) {

	// Savegame file names
	// {servernamn}-T{turn}-Y{year}-{auto/manual}.save.[bz2|xz]
	r := regexp.MustCompile(`^.*/(?P<savename>[\S\d-]+)-T(?P<turn>\d+)-Y(?P<year>-*\d+)-auto.sav.[xb]z2*`)
	match := r.FindStringSubmatch(filename)
	if len(match) == 4 {
		ok = true
		savename = match[1]

		turn, err := strconv.Atoi(match[2])
		if err != nil {
			ok = false
			turn = -1
		}
		year, err = strconv.Atoi(match[3])
		if err != nil {
			ok = false
			year = -1
		}
		return savename, turn, year, ok
	} else {
		ok = false
		return savename, turn, year, ok
	}
}
