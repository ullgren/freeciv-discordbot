package internal

import "testing"

func TestParseAutoSaveFileName(t *testing.T) {

	tables := []struct {
		filename     string
		savename     string
		expectedturn int
		expectedyear int
		isok         bool
	}{
		{"/home/freeciv/server-1-T0184-Y01340-auto.sav.bz2", "server-1", 184, 1340, true},
		{"/home/freeciv/server-1-T0001-Y-3950-auto.sav.bz2", "server-1", 1, -3950, true},
                {"/home/freeciv/baraenserver-3-T0002-Y-3900-auto.sav.bz2", "baraenserver-3", 2, -3900, true},
		// Test invalid integer for turn and year
		{"/home/freeciv/server-1-T9999999999999999999-Y01340-auto.sav.bz2", "server-1", -1, 1340, false},
		{"/home/freeciv/server-1-T0001-Y9999999999999999999-auto.sav.bz2", "server-1", 1, -1, false},
		{"/home/freeciv/server-1-T9999999999999999999-Y9999999999999999999-auto.sav.bz2", "server-1", -1, -1, false},
		// Test for manual save games
		{"/home/freeciv/server-1-T0184-Y01340-manual.sav.bz2", "", 0, 0, false},
		// Test for any other files
		{"/home/freeciv/does-no-match-pattern.txt", "", 0, 0, false},
		// freeciv 3.x saves files using xz
		{"/home/freeciv/server-1-T0184-Y01340-auto.sav.xz", "server-1", 184, 1340, true},
	}

	for index, table := range tables {
		savename, turn, year, ok := ParseAutoSaveFileName(table.filename)

		if ok != table.isok {
			if table.isok == true {
				t.Errorf("Test %d : The filename was determined to not OK but expected it to be OK.", index)
			} else {
				t.Errorf("Test %d : The filename was determined to OK but expected it to be invalid.", index)
			}
			// No point in continuing since the filename validation failed
			continue
		}
		if savename != table.savename {
			t.Errorf("Test %d : Savename was incorrect, got: %s, want: %s.", index, savename, table.savename)
		}

		if turn != table.expectedturn {
			t.Errorf("Test %d : Turn was incorrect, got: %d, want: %d.", index, turn, table.expectedturn)
		}

		if year != table.expectedyear {
			t.Errorf("Test %d : Turn was incorrect, got: %d, want: %d.", index, year, table.expectedyear)
		}
	}

}
